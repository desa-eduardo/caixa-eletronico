package com.caixaeletronico.service;

import com.caixaeletronico.vo.Notas;

public class CaixaEletronico {

    public Notas saque(int valor, Notas notas) {

        if(valor >= 100){
            notas.setNotas100(notas.getNotas100() + 1);
            valor -= 100;
        } else if(valor >= 50){
            notas.setNotas50(notas.getNotas50() + 1);
            valor -= 50;
        }  else if(valor >= 20){
            notas.setNotas20(notas.getNotas20() + 1);
            valor -= 20;
        }  else if(valor >= 10){
            notas.setNotas10(notas.getNotas10() + 1);
            valor -= 10;
        } else {
            throw new RuntimeException("Valor inválido!");
        }

        if(valor > 0){
            return this.saque(valor, notas);
        } else {
            return notas;
        }
    }

    public String mensagemFormatada(Notas notas){
        StringBuilder sb = new StringBuilder();
        sb.append("valor do Saque: " + notas.valorFinal());
        sb.append(" - Resultado:");

        if(notas.getNotas100() > 0){
            sb.append(" Entregar "+ notas.getNotas100().toString() +" nota de R$100,00");
        }
        if(notas.getNotas50() > 0){
            sb.append(" Entregar "+ notas.getNotas50().toString() +" nota de R$50,00");
        }
        if(notas.getNotas20() > 0){
            sb.append(" Entregar "+ notas.getNotas20().toString() +" nota de R$20,00");
        }
        if(notas.getNotas10() > 0){
            sb.append(" Entregar "+ notas.getNotas10().toString() +" nota de R$10,00");
        }

        return  sb.toString();
    }
}