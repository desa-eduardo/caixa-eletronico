package com.caixaeletronico.vo;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Objects;

public class Notas {

    public Notas() {
        this.notas100 = 0;
        this.notas50 = 0;
        this.notas20 = 0;
        this.notas10 = 0;
    }

    private Integer notas100;
    private Integer notas50;
    private Integer notas20;
    private Integer notas10;

    public Integer getNotas100() {
        return notas100;
    }

    public void setNotas100(Integer notas100) {
        this.notas100 = notas100;
    }

    public Integer getNotas50() {
        return notas50;
    }

    public void setNotas50(Integer notas50) {
        this.notas50 = notas50;
    }

    public Integer getNotas20() {
        return notas20;
    }

    public void setNotas20(Integer notas20) {
        this.notas20 = notas20;
    }

    public Integer getNotas10() {
        return notas10;
    }

    public void setNotas10(Integer notas10) {
        this.notas10 = notas10;
    }

    public String valorFinal(){
        Double valor = 0.0;
        valor+= getNotas100() * 100;
        valor+= getNotas50() * 50;
        valor+= getNotas20() * 20;
        valor+= getNotas10() * 10;

        Locale ptBr = new Locale("pt", "BR");
        String valorFormatado = NumberFormat.getCurrencyInstance(ptBr).format(valor);

        return valorFormatado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Notas notas = (Notas) o;
        return notas100.equals(notas.notas100) &&
                notas50.equals(notas.notas50) &&
                notas20.equals(notas.notas20) &&
                notas10.equals(notas.notas10);
    }

    @Override
    public int hashCode() {
        return Objects.hash(notas100, notas50, notas20, notas10);
    }
}
