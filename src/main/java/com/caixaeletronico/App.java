package com.caixaeletronico;

import com.caixaeletronico.service.CaixaEletronico;
import com.caixaeletronico.vo.Notas;

public class App {
    public static void main(String[] args) {
        CaixaEletronico caixaEletronico = new CaixaEletronico();
        Notas notas = new Notas();

        notas = caixaEletronico.saque(360, notas);
        System.out.println(caixaEletronico.mensagemFormatada(notas));
    }
}
