package com.caixaeletronico.vo;

import com.caixaeletronico.service.CaixaEletronico;
import com.caixaeletronico.vo.Notas;
import org.junit.Assert;
import org.junit.Test;

public class NotasTest {
    @Test
    public void testNotas_valorFinal() {
        Notas notas = new Notas();
        notas.setNotas100(5);
        notas.setNotas50(1);
        notas.setNotas20(2);
        notas.setNotas10(3);

        Assert.assertEquals("R$ 620,00", notas.valorFinal());
    }

    @Test(expected = NullPointerException.class)
    public void testNotas_valorFinal_nota_nula() {
        Notas notas = new Notas();
        notas.setNotas100(null);
        notas.setNotas50(1);
        notas.setNotas20(2);
        notas.setNotas10(3);

        notas.valorFinal();
    }
}
