package com.caixaeletronico.service;

import com.caixaeletronico.service.CaixaEletronico;
import com.caixaeletronico.vo.Notas;
import org.junit.Assert;
import org.junit.Test;

public class CaixaEletronicoTest {

    @Test
    public void testSaque_quantidadeNotas() {
        CaixaEletronico caixaEletronico = new CaixaEletronico();
        Notas notas = new Notas();

        notas = caixaEletronico.saque(380, notas);

        Assert.assertEquals(new Integer(3), notas.getNotas100());
        Assert.assertEquals(new Integer(1), notas.getNotas50());
        Assert.assertEquals(new Integer(1), notas.getNotas20());
        Assert.assertEquals(new Integer(1), notas.getNotas10());
    }

    @Test(expected = RuntimeException.class)
    public void testSaque_quantidadeNotas_invalido() {
        CaixaEletronico caixaEletronico = new CaixaEletronico();
        Notas notas = new Notas();

        notas = caixaEletronico.saque(385, notas);
    }
}
