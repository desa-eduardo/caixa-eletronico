Descrição da solução:

Pacotes:

    Separei os pacotes com
                        com.caixaeletronico -> Para rodar a aplicação com uma classe App onde está o método main
                        com.caixaeletronico.service -> esse pacote serve para colocar todas classes com regras de negócio especifica
                        com.caixaeletronico.vo -> todas classes de resposta

    os testes seguem na mesta estrutura dos pacotes acima, so que dentro da pasta src/test/java

Rodar:

    Para build e rodar os testes utilize o comando -> mvn clean install(dentro da pasta do projeto onde está o pom.xml)
    Para rodar a aplicação -> java -jar caixaeletronico-1.0-SNAPSHOT.jar(dentro da pasta target gerada pelo comando anterior)

Explicação:

    No metódo principal, fiz uma condição onde retirava primeiro as notas de maior valor até chegar nas de menor valor, dessa forma sempre
    irá entregar o menor número possível de notas em cada saque.